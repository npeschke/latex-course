# Skript und Präsentation für den LaTeX-Kurs
Autor: Nicolas Peschke


## Benötigte Software
### Lokale installation
- LaTeX Distribution (z.B. MikTeX, TeXlive, etc.)
- Editor TeXstudio
- Literaturverwaltung (z.B. Zotero, Citavi, etc.)
- Git (optional)

### Alternativ: Online
Ihr könnt selbstverständlich auch Overleaf o.ä. benutzen, die Vorlagen etc.
funktionieren auch damit, jedoch stelle ich in dem Kurs die lokale Installation vor.

## Installation
### Literaturverwaltung (nur eine benötigt)
#### Zotero
1. Zotero herunterladen https://www.zotero.org/download/
1. und installieren (Hilfe gibts hier: https://www.zotero.org/support/installation)
1. Einstellungen anpassen (siehe Teil 1 der [Präsentationsfolien](presentation/latex_kurs.pdf))

#### Citavi
1. Citavi herunterladen (https://www.citavi.com/de/download)
1. Lizenz beziehen (http://www.citavi.com/uni-heidelberg)
1. Citavi starten und mit dem erstellten Konto anmelden
1. Einstellungen anpassen (siehe Teil 1 der [Präsentationsfolien](presentation/latex_kurs.pdf))

**Nach der Installation der Literaturverwaltung müsst ihr noch ein paar Einstellungen anpassen.**
**Die Anleitung dafür findet ihr in Teil 1 der [Präsentationsfolien](presentation/latex_kurs.pdf)**

## Git

- Betriebssystem hier auswählen
https://git-scm.com/downloads
- Installationsanweisungen folgen

## LaTeX
**WICHTIG BEI MIKTEX: Wenn ihr nach den Settings gefragt werdet: umbedingt "_always_" statt "_ask me first_" auswählen!**

![Wenn ihr nach den Settings gefragt werdet: umbedingt "_always_" statt "_ask me first_" auswählen!](https://miktex.org/img/basic-miktex-settings.png?width=400)

Folgt den jeweiligen HOWTOs auf https://miktex.org/ für euer Betriebssystem

*Windows*: https://miktex.org/howto/install-miktex

*Linux*: Hier empfehle ich TeXlive, das sollte bei den meisten Distributionen `texlive-full` Paket verfügbar sein Alternativ ist auch MikTeX möglich

*Mac*: https://miktex.org/howto/install-miktex-mac Alternativ: https://www.tug.org/mactex/

### TeXstudio
Unter https://www.texstudio.org/#download die entsprechende Version für euer Betriebssystem auswählen und installieren
